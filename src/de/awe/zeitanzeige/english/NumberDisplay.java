package de.awe.zeitanzeige.english;

public class NumberDisplay {
    private final int limit;
    private int value;

    /**
     * Konstruktor für Exemplare der Klasse Nummernanzeige.
     * Setzt das Limit, bei dem die Anzeige zurückgesetzt wird.
     */
    public NumberDisplay(int displayLimit) {
        this.limit = displayLimit;
        this.value = 0;
    }

    /**
     * Liefere den aktuellen Wert als int.
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Liefere den Anzeigewert, also den Wert dieser Anzeige als
     * einen String mit zwei Ziffern. Wenn der Wert der Anzeige
     * kleiner als zehn ist, wird die Anzeige mit einer führenden
     * Null eingerückt.
     */
    public String getDisplayValue() {
        if (this.value <= 9) {
            return "0" + this.value;
        } else {
            return "" + this.value;
        }
    }

    /**
     * Setze den Wert der Anzeige auf den angegebenen 'replacementValue'.
     * Wenn der angegebene Wert unter Null oder über dem Limit liegt,
     * tue nichts.
     */
    public void setValue(int replacementValue) {
        if ((replacementValue >= 0) && (replacementValue < this.limit)) {
            this.value = replacementValue;
        }
    }

    /**
     * Erhöhe den Wert um Eins. Wenn das Limit erreicht ist, setze
     * den Wert wieder auf Null.
     */
    public void increase() {
        this.value = (this.value + 1) % this.limit;
    }
}
