package de.awe.zeitanzeige.english;

public class ClockDisplay {
    private final NumberDisplay hours;
    private final NumberDisplay minutes;
    private String timeDisplay;    // simuliert die tatsächliche Anzeige

    /**
     * Konstruktor für ein Exemplar von Uhrenanzeige.
     * Mit diesem Konstruktor wird die Anzeige auf 00:00 initialisiert.
     */
    public ClockDisplay() {
        this.hours = new NumberDisplay(24);
        this.minutes = new NumberDisplay(60);
        this.displayUpdate();
    }

    /**
     * Konstruktor für ein Exemplar von Uhrenanzeige.
     * Mit diesem Konstruktor wird die Anzeige auf den Wert
     * initialisiert, der durch 'hour' und 'minute'
     * definiert ist.
     */
    public ClockDisplay(int hour, int minute) {
        this.hours = new NumberDisplay(24);
        this.minutes = new NumberDisplay(60);
        this.setTime(hour, minute);
    }

    /**
     * Diese Operation sollte einmal pro Minute aufgerufen werden -
     * sie sorgt dafür, dass diese Uhrenanzeige um eine Minute
     * weiter gestellt wird.
     */
    public void getClockSignal() {
        this.minutes.increase();
        if (this.minutes.getValue() == 0) {  // Limit wurde erreicht!
            this.hours.increase();
        }
        this.displayUpdate();
    }

    /**
     * Setze die Uhrzeit dieser Anzeige auf die gegebene 'hour' und
     * 'minute'.
     */
    public void setTime(int hour, int minute) {
        this.hours.setValue(hour);
        this.minutes.setValue(minute);
        this.displayUpdate();
    }

    /**
     * Liefere die aktuelle Uhrzeit dieser Uhrenanzeige im Format SS:MM.
     */
    public String getTime() {
        return this.timeDisplay;
    }

    /**
     * Aktualisiere die interne Zeichenkette, die die Zeitanzeige hält.
     */
    private void displayUpdate() {
        this.timeDisplay = this.hours.getDisplayValue() + ":"
                + this.minutes.getDisplayValue();
    }
}
